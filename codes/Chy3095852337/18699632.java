/**
 * 冒泡排序函数
 * aa bb cc
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int[] a, int n) {
    // 外层循环控制排序的总轮数，轮数等于数组长度减一
    for (int i = 0; i < n - 1; i++) {
        // 初始化一个标志位，用于判断当前轮是否发生了交换
        boolean swapped = false;

        // 内层循环控制每一轮的比较次数，每次比较相邻的两个元素
        for (int j = 0; j < n - i - 1; j++) {
            // 如果当前元素大于下一个元素，交换它们
            if (a[j] > a[j + 1]) {
                // 交换元素
                int temp = a[j];
                a[j] = a[j + 1];
                a[j + 1] = temp;

                // 设置标志位为 true
                swapped = true;
            }
        }

        if (!swapped) {
            break;
        }
    }
}

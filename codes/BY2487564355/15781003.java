/**  
 * 冒泡排序函数  
 * 通过重复遍历待排序的数组，比较相邻的元素，如果它们的顺序错误就把它们交换过来。  
 * 遍历数组的工作是重复地进行直到没有再需要交换，也就是说该数组已经排序完成。  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int [] a, int n){  
    for (int i = 0; i < n - 1; i++) {  
        for (int j = 0; j < n - i - 1; j++) {  
            // 如果前一个元素大于后一个元素，则交换它们的位置  
            if (a[j] > a[j + 1]) {  
                // 交换 a[j] 和 a[j + 1]  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} //end

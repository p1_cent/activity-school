/**  
 * 冒泡排序函数  
 * 该函数会对数组a进行排序，使其变为有序数组  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int [] a, int n){  
    for (int i = 0; i < n - 1; i++) { // 外层循环控制排序趟数  
        for (int j = 0; j < n - 1 - i; j++) { // 内层循环控制每一趟排序多少次  
            if (a[j] > a[j + 1]) { // 如果前一个数比后一个数大，则交换这两个数  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} //end

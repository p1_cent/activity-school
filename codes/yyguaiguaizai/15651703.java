/**  
 * 冒泡排序函数  
 * 该函数用于对一个整型数组进行冒泡排序  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int [] a, int n){  
    // 冒泡排序的主体部分  
    for (int i = 0; i < n - 1; i++) {  
        // 设置一个标志位，表示本趟是否发生过交换  
        boolean swapped = false;  
        for (int j = 0; j < n - i - 1; j++) {  
            // 如果前一个元素大于后一个元素，则交换它们  
            if (a[j] > a[j + 1]) {  
                // 交换 a[j] 和 a[j+1]  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
                // 发生了交换，标志位设为 true  
                swapped = true;  
            }  
        }  
        // 如果本趟没有发生交换，说明数组已经有序，可以提前结束排序  
        if (!swapped) {  
            break;  
        }  
    }  
} //end

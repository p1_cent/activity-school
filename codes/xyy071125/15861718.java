/**
 * 冒泡排序函数
 * aa bb cc
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int [] a, int n){
    // 你的代码，使无序数组 a 变得有序
    for(int i = 0; i < n; i++) {
	    for(int liuy = 0; liuy < n - i - 1; liuy++) {
		    if(a[liuy] > a[liuy + 1]) {
			    int temp = a[liuy];
			    a [liuy] = a[liuy + 1];
			    a[liuy + 1] = temp;
		    }
	    }
    }


} //end,liuy
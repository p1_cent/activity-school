/**  
 * 冒泡排序函数  
 * 通过相邻元素之间的比较和交换，将无序数组变为有序数组  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int [] a, int n){  
    for (int i = 0; i < n - 1; i++) {  
        // 标记变量，用于优化，如果在一趟排序中没有交换，则数组已经有序  
        boolean swapped = false;  
        for (int j = 0; j < n - i - 1; j++) {  
            // 如果前一个元素大于后一个元素，则交换它们  
            if (a[j] > a[j + 1]) {  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
                // 发生了交换，将标记设为true  
                swapped = true;  
            }  
        }  
        // 如果在内层循环中没有发生交换，则数组已经有序，提前退出  
        if (!swapped) {  
            break;  
        }  
    }  
} //end

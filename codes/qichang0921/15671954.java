/**  
 * 冒泡排序函数  
 * 通过重复遍历待排序的数组，比较相邻的元素并交换它们（如果它们在错误的顺序），  
 * 直到没有更多的交换需要，即该数组已经排序。  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) { // 外层循环控制排序的趟数  
        boolean swapped = false; // 用于标记某一趟是否发生了交换  
        for (int j = 0; j < n - i - 1; j++) { // 内层循环控制每一趟排序多少次  
            if (a[j] > a[j + 1]) { // 如果前一个元素大于后一个元素，则交换它们  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
                swapped = true; // 标记发生了交换  
            }  
        }  
        if (!swapped) { // 如果某一趟没有发生交换，说明数组已经有序，可以提前退出  
            break;  
        }  
    }  
} // end

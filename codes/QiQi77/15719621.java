/**  
 * 冒泡排序函数  
 * 通过相邻元素之间的比较和交换，将最大的元素“冒泡”到数组的末尾  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int [] a, int n){  
    for (int i = 0; i < n - 1; i++) {  
        // 标记是否有交换发生，用于优化，如果这一趟没有交换，说明已经有序  
        boolean swapped = false;  
        for (int j = 0; j < n - i - 1; j++) {  
            // 如果当前元素大于下一个元素，则交换它们  
            if (a[j] > a[j + 1]) {  
                // 交换元素  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
                // 标记发生了交换  
                swapped = true;  
            }  
        }  
        // 如果没有发生交换，说明数组已经有序，可以提前结束排序  
        if (!swapped) {  
            break;  
        }  
    }  
} //end

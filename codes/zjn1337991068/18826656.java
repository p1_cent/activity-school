/**
 * 冒泡排序函数
 * aa bb cc
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int [] a, int n){
    // 你的代码，使无序数组 a 变得有序
    if (n <= 1) return; // 如果数组长度小于等于1，则不需要排序

    for (int i = 0; i < n; ++i) {
        // 每次遍历都会把未排序部分的最大值放到正确的位置
        for (int j = 0; j < n - i - 1; ++j) {
            // 如果前一个元素比后一个元素大，则交换它们的位置
            if (a[j] > a[j + 1]) {
                int temp = a[j];
                a[j] = a[j + 1];
                a[j + 1] = temp;
            }
        }
    }

} //end

/**
 * 冒泡排序函数
 * 对数组进行从小到大的排序
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int[] a, int n) {
    // 外层循环控制遍历的次数
    for (int i = 0; i < n - 1; i++) {
        // 提前退出标志位，如果本次没有交换，说明已经有序
        boolean swapped = false;
        
        // 内层循环进行两两比较
        for (int j = 0; j < n - 1 - i; j++) {
            // 如果前一个数大于后一个数，则交换
            if (a[j] > a[j + 1]) {
                // 交换 a[j] 和 a[j + 1]
                int temp = a[j];
                a[j] = a[j + 1];
                a[j + 1] = temp;
                
                // 标记有交换
                swapped = true;
            }
        }
        
        // 如果没有发生交换，说明数组已经是有序的，提前结束循环
        if (!swapped) {
            break;
        }
    }
} //end


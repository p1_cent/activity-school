public static void bubbleSort(int []a,int n){
	boolean swap;
	for(int i=0;i<n-1;i++){
		swap = false;
		for(int j=0;j<n-1-i;j++){
			if(a[j]>a[j+1]){
				int temp = a[j];
				a[i] = a[j+1];
				a[j+1] = temp;
				swap = true;
			}
		}
		if(!swap){
			break;
		}
	}
}


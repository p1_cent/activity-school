/**
 * 冒泡排序函数
 * 对传入的数组进行排序，使得数组中的元素按照升序排列
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int [] a, int n){
    // 外层循环控制排序的总轮数，每轮排序后最大的元素会冒泡到它应该在的位置
    for (int i = 0; i < n - 1; i++) {
        // 内层循环进行每轮的元素比较和交换
        for (int j = 0; j < n - 1 - i; j++) {
            // 如果当前元素大于下一个元素，则交换它们
            if (a[j] > a[j + 1]) {
                // 交换 a[j] 和 a[j + 1]
                int temp = a[j];
                a[j] = a[j + 1];
                a[j + 1] = temp;
            }
        }
    }
    // 排序完成，无需返回值，因为数组 a 已经被原地修改
} //end

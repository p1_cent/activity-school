/**  
 * 冒泡排序函数  
 * 遍历数组，比较相邻元素，如果顺序错误则交换它们，重复这个过程直到没有需要交换的元素为止。  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int [] a, int n){  
    for (int i = 0; i < n - 1; i++) {  
        // 标志位，用于标记本趟是否有交换，如果没有交换则已经有序，无需再继续比较  
        boolean swapped = false;  
        for (int j = 0; j < n - i - 1; j++) {  
            // 如果前一个元素大于后一个元素，则交换它们  
            if (a[j] > a[j + 1]) {  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
                // 发生了交换，标志位设为true  
                swapped = true;  
            }  
        }  
        // 如果没有交换，则说明数组已经有序，无需继续后面的趟  
        if (!swapped) {  
            break;  
        }  
    }  
} //end
